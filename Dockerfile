FROM php:7-fpm-alpine

LABEL maintainer="Pantomath SAS <hello@pantomath.io>"

RUN sed -ri -e 's/;pm\.status_path/pm.status_path/g' /usr/local/etc/php-fpm.d/www.conf

