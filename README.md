# docker-php-fpm-with-status

This is a Docker image for php-fpm, based on the official alpine image (see [official repository](https://github.com/docker-library/docs/blob/master/php/README.md)), but with [status](http://php.net/manual/en/install.fpm.php) configured.

## How it works

Please refer to the [official image documentation](https://hub.docker.com/_/php/). The only difference here is you can access the URI `/status` to get the basic status information.